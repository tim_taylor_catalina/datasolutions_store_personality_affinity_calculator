# -*- coding: utf-8 -*-
"""
Created on Mon Dec 10 13:35:23 2018

@author: ttaylor
"""

                          
def writelogger( strMessage ):
    from datetime import datetime
    from datetime import date
    with open(r'\\stphome\home\ttaylor\logs\store_personalities_calculator ' + str(date.today()) + ' .txt','a') as logger:
        logger.write(str(datetime.now()) + '   ' + strMessage + '\n')
        

def process_store_personality_affinities(intStart,intEnd ):
    strSQL = "delete from tt_affinity_store_personality_results  "
    cursorStore.execute(strSQL) 
    connStore.commit()
    writelogger("Store Personality affinities truncated")

    
    strSQL = "select distinct store_personality_group, store_personality, store_personality_strength "
    strSQL = strSQL  +  "from tt_store_personality_master  "
    strSQL = strSQL  +  "WHERE store_personality_strength = 'strong'  "
    strSQL = strSQL  +  "AND store_personality not in (select distinct store_personality from tt_affinity_store_personality_results) "
    dfStorePer = pd.read_sql(strSQL, connStore)
    

    for index, row in dfStorePer.iterrows():  
        writelogger("processsing " + row['store_personality_group'] + ' <> ' +   row['store_personality'] + ' <> ' +  row['store_personality_strength'])
    
        ####--get core customers
        strSQL = "drop table tt_store_personality_shoppers if exists; "
        strSQL = strSQL  +  "CREATE TEMP TABLE tt_store_personality_shoppers AS  "
        strSQL = strSQL  +  "SELECT distinct o.ord_designated_cnsmr_id_key,  "
        strSQL = strSQL  +  "		store_personality_group, store_personality, store_personality_strength, "
        strSQL = strSQL  +  "		sum(purch_amt) as attribute_spend "
        strSQL = strSQL  +  "FROM PN1USPA1..ord_trd_itm_fact_ne_v o "
        strSQL = strSQL  +  "JOIN PN1USPA1..touchpoint_v p "
        strSQL = strSQL  +  "	ON p.touchpoint_key = o.ord_touchpoint_key "
        strSQL = strSQL  +  "JOIN tt_store_personality_master   q "
        strSQL = strSQL  +  "	on p.site_key = q.site_key "
        strSQL = strSQL  +  "	AND  store_personality_group = '" +  row['store_personality_group'] + "' "
        strSQL = strSQL  +  "	and store_personality = '" +  row['store_personality'] + "' "
        strSQL = strSQL  +  "	and store_personality_strength = '" +   row['store_personality_strength'] + "' "
        strSQL = strSQL  +  "join PN1USPA1..consumer_id_v c  "
        strSQL = strSQL  +  "	on o.ord_designated_cnsmr_id_key = c.cnsmr_id_key "
        strSQL = strSQL  +  "	AND c.unident_ind = 'N' "
        strSQL = strSQL  +  "	AND c.abuser_ind = 'N' "
        strSQL = strSQL  +  "JOIN PN1USPA1..trade_item_v u "
        strSQL = strSQL  +  "	on o.trade_item_key = u.trade_item_key "
        strSQL = strSQL  +  "WHERE  o.ord_date_key between " + str(intStart) + " and " + str(intEnd) + " "
        strSQL = strSQL  +  "AND ord_designated_cnsmr_id_key > 0  "
        strSQL = strSQL  +  "AND purch_amt > 0 "
        strSQL = strSQL  +  "AND purch_qty > 0  "
        strSQL = strSQL  +  "GROUP BY  o.ord_designated_cnsmr_id_key, store_personality_group, store_personality, store_personality_strength    "
        cursorStore.execute(strSQL) 
        connStore.commit()
        writelogger("        Store Personality Shoppers found ")
            
        strSQL =   "	drop table tt_store_personality_summary if exists; "
        strSQL = strSQL  +  "	CREATE TEMP TABLE tt_store_personality_summary AS  "
        strSQL = strSQL  +  "	SELECT  q.store_personality_group, q.store_personality, q.store_personality_strength, count(distinct ord_event_key) as buyer_group_trips "
        strSQL = strSQL  +  "	FROM PN1USPA1..ord_trd_itm_fact_ne_v o "
        strSQL = strSQL  +  "	JOIN PN1USPA1..touchpoint_v p "
        strSQL = strSQL  +  "		ON p.touchpoint_key = o.ord_touchpoint_key "
        strSQL = strSQL  +  "	JOIN tt_store_personality_master   q "
        strSQL = strSQL  +  "		on p.site_key = q.site_key "
        strSQL = strSQL  +  "	AND  store_personality_group = '" +  row['store_personality_group'] + "' "
        strSQL = strSQL  +  "	and store_personality = '" +  row['store_personality'] + "' "
        strSQL = strSQL  +  "	and store_personality_strength = '" +   row['store_personality_strength'] + "' "
        strSQL = strSQL  +  "	JOIN tt_store_personality_shoppers as c "
        strSQL = strSQL  +  "		ON c.ord_designated_cnsmr_id_key = o.ord_designated_cnsmr_id_key "
        strSQL = strSQL  +  "		AND q.store_personality_group = c.store_personality_group "
        strSQL = strSQL  +  "		AND q.store_personality = c.store_personality "
        strSQL = strSQL  +  "		AND q.store_personality_strength = c.store_personality_strength "
        strSQL = strSQL  +  "WHERE  o.ord_date_key between " + str(intStart) + " and " + str(intEnd) + " "
        strSQL = strSQL  +  "	group by q.store_personality_group, q.store_personality, q.store_personality_strength "
        strSQL = strSQL  +  "	DISTRIBUTE ON RANDOM; "
        cursorStore.execute(strSQL) 
        connStore.commit()
        writelogger("        Store Personality Trips found ")


        
        strSQL = "drop table tt_both_brand if exists; "
        strSQL = strSQL  +  "CREATE TABLE tt_both_brand AS  "
        strSQL = strSQL  +  "SELECT q.store_personality_group, q.store_personality, q.store_personality_strength,  "
        strSQL = strSQL  +  "	brand_desc, cat_desc, mfg_parent_corp_nm,     "
        strSQL = strSQL  +  "	count(distinct ord_event_key) as both_count, "
        strSQL = strSQL  +  "	count(distinct o.ord_designated_cnsmr_id_key) as both_household_buyers    "
        strSQL = strSQL  +  "FROM PN1USPA1..ord_trd_itm_fact_ne_v o "
        strSQL = strSQL  +  "JOIN PN1USPA1..touchpoint_v p "
        strSQL = strSQL  +  "	ON p.touchpoint_key = o.ord_touchpoint_key "
        strSQL = strSQL  +  "JOIN tt_store_personality_master   q "
        strSQL = strSQL  +  "	on p.site_key = q.site_key   "
        strSQL = strSQL  +  "	AND  q.store_personality_group = '" +  row['store_personality_group'] + "' "
        strSQL = strSQL  +  "	and q.store_personality = '" +  row['store_personality'] + "' "
        strSQL = strSQL  +  "	and q.store_personality_strength = '" +   row['store_personality_strength'] + "' "
        strSQL = strSQL  +  "JOIN PN1USPA1..trade_item_v u "
        strSQL = strSQL  +  "	on o.trade_item_key = u.trade_item_key "
        strSQL = strSQL  +  "JOIN tt_store_personality_shoppers as c "
        strSQL = strSQL  +  "	ON c.ord_designated_cnsmr_id_key = o.ord_designated_cnsmr_id_key "
        strSQL = strSQL  +  "	AND q.store_personality_group = c.store_personality_group "
        strSQL = strSQL  +  "	AND q.store_personality = c.store_personality "
        strSQL = strSQL  +  "	AND q.store_personality_strength = c.store_personality_strength "
        strSQL = strSQL  +  "WHERE  o.ord_date_key between " + str(intStart) + " and " + str(intEnd) + " "
        strSQL = strSQL  +  "GROUP BY q.store_personality_group, q.store_personality, q.store_personality_strength, brand_desc, cat_desc, mfg_parent_corp_nm  "
        strSQL = strSQL  +  "DISTRIBUTE ON (brand_desc, cat_desc, mfg_parent_corp_nm); "
        cursorStore.execute(strSQL) 
        connStore.commit()
        writelogger("        Store Personality Brand Purchases Captured ")

        strSQL =  "insert into tt_affinity_store_personality_results "
        strSQL = strSQL  +  "select x.*, brand_trips as product_b_trips, total_trips,	buyer_group_trips,  "
        strSQL = strSQL  +  "	(both_count::float * total_trips::float) / (brand_trips::float * buyer_group_trips::float) as affinity "
        strSQL = strSQL  +  "from tt_both_brand x "
        strSQL = strSQL  +  "join tt_store_personality_summary a "
        strSQL = strSQL  +  " 	on a.store_personality_group = x.store_personality_group "
        strSQL = strSQL  +  "	and a.store_personality = x.store_personality "
        strSQL = strSQL  +  "	and a.store_personality_strength = x.store_personality_strength "
        strSQL = strSQL  +  "join tt_affinity_xref_brand_totals b "
        strSQL = strSQL  +  "	on x.brand_desc = b.brand_desc "
        strSQL = strSQL  +  "	and x.cat_desc = b.cat_desc  "
        strSQL = strSQL  +  "	and x.mfg_parent_corp_nm = b.mfg_parent_corp_nm "
        strSQL = strSQL  +  "	and b.mega_attribute = 'all' "
        strSQL = strSQL  +  "join tt_affinity_xref_totals t "
        strSQL = strSQL  +  "	on both_count < total_trips "
        strSQL = strSQL  +  "	and t.mega_attribute = 'all' "
        strSQL = strSQL  +  "where product_b_trips > 100000 "
        cursorStore.execute(strSQL) 
        connStore.commit()
        writelogger("        Store Personality Affinities Saved ")
        
        
writelogger("all done!")        

####-------------------------------------------------------------
####  main function
###
###
#connect to Netezza
import sys
import pandas as pd         #import panda
sys.path.append(r'\\stphome\home\ttaylor\python')
from connect import UID,PWD    #import your netezza ID/PWD. This must be in the same location as your code.
connect_string="DRIVER={NetezzaSQL}"+";SERVER=prodnzo.catmktg.com; PORT=5480; DATABASE=pn1ussa1;UID={uid};PWD={pwd}".format(uid=UID, pwd=PWD)
#import connection package and establish connection
import pyodbc                  #import package
connStore = pyodbc.connect(connect_string)  #establish a connection
cursorStore = connStore.cursor()         #open a cursor                                   


process_store_personality_affinities(20171201, 20181130)